from aiogram import Bot, Dispatcher, types
from aiogram.contrib.fsm_storage.memory import MemoryStorage
from aiogram.dispatcher import FSMContext
from aiogram.utils import executor
from aiogram.dispatcher.filters.state import State, StatesGroup
from aiogram.types import InlineKeyboardMarkup, InlineKeyboardButton
import sqlite3
import gspread


BOT_TOKEN = "1675870180:AAGlk22K8pTZuEdma1dy8ybp4ucatsh0d54"

API_TOKEN = BOT_TOKEN

bot = Bot(token=API_TOKEN)

storage = MemoryStorage()
dp = Dispatcher(bot, storage=storage)

conn = sqlite3.connect('feedback.db')
cur = conn.cursor()
cur.execute("""CREATE TABLE IF NOT EXISTS answers(
                id INTEGER PRIMARY KEY NOT NULL,
                q1 INTEGER,
                q2 INTEGER,
                q3 INTEGER,
                q4 INTEGER,
                q5 TEXT,
                time TEXT,
                UNIQUE(id));
                """)
conn.commit()


class Mention(StatesGroup):
    start = State()
    q1 = State()
    q2 = State()
    q3 = State()
    q4 = State()
    q5 = State()

ans = (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
qs = ["1\. *Какова вероятность того, что Вы порекомендуете магазин своим знакомым?*\nпо 10\-бальной шкале\n_0 — «Ни в коем случае не буду рекомендовать»_\n_7 — «Скорее всего порекомендую»_\n_10 — «Обязательно порекомендую»_",
    "2\. *Как Вы оцените ассортимент магазина?*\nпо 10\-балльной шкале\n_0 — «Ужасно»_\n_7 — «Нормально»_\n_10 — «Очень хороший»_",
    "3\. *Как Вы оцените обслуживание в  магазине?*\nпо 10\-балльной шкале\n_0 — «Ужасно»_\n_7 — «Нормально»_\n_10 — «Очень хорошее»_",
    "4\. *Как Вы оцените цены товаров?*\nпо 10\-балльной шкале\n_0 — «Очень дорого»_\n_7 — «Нормальные»_\n_10 — «Отличные»_",
    "5\. *Что нам улучшить, чтобы Вы рекомендовали нас Вашим знакомым?*\n_\(напишите ваш ответ ниже\)_"]


async def inline_kb():
    bt0 = InlineKeyboardButton('0🤮', callback_data='bt0')
    bt1 = InlineKeyboardButton('1', callback_data='bt1')
    bt2 = InlineKeyboardButton('2', callback_data='bt2')
    bt3 = InlineKeyboardButton('3', callback_data='bt3')
    bt4 = InlineKeyboardButton('4', callback_data='bt4')
    bt5 = InlineKeyboardButton('5', callback_data='bt5')
    bt6 = InlineKeyboardButton('6', callback_data='bt6')
    bt7 = InlineKeyboardButton('7😚', callback_data='bt7')
    bt8 = InlineKeyboardButton('8', callback_data='bt8')
    bt9 = InlineKeyboardButton('9', callback_data='bt9')
    bt10 = InlineKeyboardButton('10😍', callback_data='bt10')
    inline_kb = InlineKeyboardMarkup()
    inline_kb.row(bt0, bt1, bt2, bt3, bt4, bt5)
    inline_kb.row(bt6, bt7, bt8, bt9, bt10)
    return inline_kb

async def edit_db(a, b, c):
    conn = sqlite3.connect('feedback.db')
    cur = conn.cursor()
    cur.execute("""UPDATE answers
                    SET %s = '%s'
                    WHERE id = '%s';
                    """ % (a, b, c))
    conn.commit()


async def append_google_sheet(id):
    gc = gspread.service_account(filename='credentials.json')
    sh = gc.open_by_key('1IqNKiCJVGe__PHDbnU5CUvLL7LWtjR8mRYnnUiffJm4')
    ws = sh.sheet1
    conn = sqlite3.connect('feedback.db')
    cur = conn.cursor()
    cur.execute("""
                SELECT * from answers
                WHERE id='%s';
                    """ % id)
    ans = cur.fetchall()
    ws.delete_rows(ws.row_count)
    ws.append_row(ans[0])
    n = 0
    mean1 = 0
    mean2 = 0
    mean3 = 0
    mean4 = 0
    cur.execute("""
            SELECT q1, q2, q3, q4 from answers;
                """)
    ans2 = cur.fetchall()
    for i in ans2:
        n += 1
        mean1 += i[0]
        mean2 += i[1]
        mean3 += i[2]
        mean4 += i[3]
    ws.append_row(('Средние:', mean1 / n, mean2 / n,
    mean3 / n, mean4 / n, '-', '-'))
    conn.commit()


@dp.message_handler(commands=['start'])
async def on_start(message: types.Message, state: FSMContext):
    id = message.from_user.id
    conn = sqlite3.connect('feedback.db')
    cur = conn.cursor()
    cur.execute("""SELECT EXISTS(
                    SELECT *
                    FROM answers
                    WHERE id = '%s'
                    LIMIT 1);
                    """ % id)
    buff = cur.fetchall()[0]
    conn.commit()
    print(ans)
    if str(buff[0]) == '0':
        button_start = InlineKeyboardButton('Начать!', callback_data='start')
        kb = InlineKeyboardMarkup()
        kb.add(button_start)

        await Mention.start.set()
        await message.reply("Привет\!\n*Предлагаю пройти небольшой опрос \(15 сек\)\!*", reply=False, reply_markup=kb, parse_mode='MarkdownV2')
    else:
        await message.reply("Еще раз привет\!\n*К сожалению отзыв можно оставить только один раз\!*", reply=False, parse_mode='MarkdownV2')

@dp.callback_query_handler(text='start', state=Mention.start)
async def question1(call: types.CallbackQuery, state: FSMContext):
    await call.message.edit_reply_markup()
    conn = sqlite3.connect('feedback.db')
    cur = conn.cursor()
    cur.execute("""INSERT OR IGNORE INTO answers(id)
                        VALUES ('%s');
                        """ % call.from_user.id)
    conn.commit()

    kb = await inline_kb()
    await Mention.q1.set()
    await call.message.edit_text(qs[0], reply_markup=kb, parse_mode='MarkdownV2')


@dp.callback_query_handler(state=Mention.q1)
async def question2(call: types.CallbackQuery, state: FSMContext):
    await call.message.edit_reply_markup()
    callback_data = call.data
    await call.answer('Вы выбрали вариант ' + callback_data[2:])
    await edit_db('q1', callback_data[2:], call.from_user.id)
    kb = await inline_kb()
    await Mention.q2.set()
    await call.message.edit_text(qs[1], reply_markup=kb, parse_mode='MarkdownV2')

@dp.callback_query_handler(state=Mention.q2)
async def question3(call: types.CallbackQuery, state: FSMContext):
    await call.message.edit_reply_markup()
    callback_data = call.data
    await call.answer('Вы выбрали вариант ' + callback_data[2:])
    await edit_db('q2', callback_data[2:], call.from_user.id)
    kb = await inline_kb()
    await Mention.q3.set()
    await call.message.edit_text(qs[2], reply_markup=kb, parse_mode='MarkdownV2')

@dp.callback_query_handler(state=Mention.q3)
async def question4(call: types.CallbackQuery, state: FSMContext):
    await call.message.edit_reply_markup()
    callback_data = call.data
    await call.answer('Вы выбрали вариант ' + callback_data[2:])
    await edit_db('q3', callback_data[2:], call.from_user.id)
    kb = await inline_kb()
    await Mention.q4.set()
    await call.message.edit_text(qs[3], reply_markup=kb, parse_mode='MarkdownV2')

@dp.callback_query_handler(state=Mention.q4)
async def question5(call: types.CallbackQuery, state: FSMContext):
    await call.message.edit_reply_markup()
    callback_data = call.data
    await call.answer('Вы выбрали вариант ' + callback_data[2:])
    await edit_db('q4', callback_data[2:], call.from_user.id)
    await Mention.q5.set()
    await call.message.edit_text(qs[4], parse_mode='MarkdownV2')


@dp.message_handler(content_types=['text'], state=Mention.q5)
async def on_end(message: types.Message, state: FSMContext):
    await edit_db('q5', message.text, message.from_user.id)
    await edit_db('time', message.date, message.from_user.id)
    await state.finish()
    await message.reply("*Спасибо за отзыв!*\nПокажите экран смартфона кассиру и заберите подарок 🎁\n_(билет на «миллион» лотереи «Сто лото» или чупа-чупс)_", reply=False, parse_mode='Markdown')
    await append_google_sheet(message.from_user.id)


@dp.message_handler(commands=['showdb'])
async def show_users(message: types.Message):
    conn = sqlite3.connect('feedback.db')
    cur = conn.cursor()
    cur.execute("""
                SELECT * from answers;
                    """)
    ans = cur.fetchall()
    conn.commit()
    await message.reply(ans, reply=False)


@dp.message_handler(commands=['stats'])
async def show_stats(message: types.Message):
    conn = sqlite3.connect('feedback.db')
    cur = conn.cursor()
    cur.execute("""
                SELECT q1, q2, q3, q4 from answers;
                    """)
    ans = cur.fetchall()
    conn.commit()

    promoters_n = (9, 10)
    detractors_n = (0, 1, 2, 3, 4, 5, 6)
    n = 0
    promoters = 0
    detractors = 0
    mean1 = 0
    mean2 = 0
    mean3 = 0
    mean4 = 0
    for i in ans:
        n += 1
        mean1 += i[0]
        mean2 += i[1]
        mean3 += i[2]
        mean4 += i[3]
        if int(i[0]) in promoters_n:
            promoters += 1
        elif int(i[0]) in detractors_n:
            detractors += 1
    buff = int((promoters - detractors) * 100 / n)
    answer = 'NPS по первому вопросу: *%s*\n' % str(buff)
    if buff > 50:
        answer += '_(Всё чудесно, но расслабляться всё равно нельзя!)_\n'
    elif 30 <= buff <= 50:
        answer += '_(неплохо, но можно и лучше)_\n'
    elif 0 <= buff < 30:
        answer += '_(недобрый знак)_\n'
    elif buff < 0:
        answer += '_(пора срочно принимать меры)_\n'
    answer += 'Усредненные значения столбцов:\n'
    answer += '1 - *%s*' % str(round(mean1 / n, 1))
    answer += '\n2 - *%s*' % str(round(mean2 / n, 1))
    answer += '\n3 - *%s*' % str(round(mean3 / n, 1))
    answer += '\n4 - *%s*' % str(round(mean4 / n, 1))
    button1 = InlineKeyboardButton('Показать рекомендации посетителей', callback_data='feedbacks')
    kb = InlineKeyboardMarkup()
    kb.add(button1)
    await message.reply(answer, reply=False, parse_mode='Markdown', reply_markup=kb)

@dp.callback_query_handler(text='feedbacks')
async def show_feedbacks(call: types.CallbackQuery):
    await call.message.edit_reply_markup()
    conn = sqlite3.connect('feedback.db')
    cur = conn.cursor()
    cur.execute("""
                SELECT q5, time from answers;
                    """)
    ans = cur.fetchall()
    conn.commit()
    answer = 'Рекомендации посетителей:\n'
    for i in ans:
        answer += '_(%s)_ *%s*\n' % (i[1], i[0])
    await call.message.reply(answer, reply=False, parse_mode='Markdown')


if __name__ == "__main__":
    executor.start_polling(dp, skip_updates=True)

